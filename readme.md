# Compilando manualmente

Para compilar o arquivo OlaDevDojo.java:  
`javac OlaDevDojo.java`  

É gerado um arquivo bytecode chamado OlaDevDojo.class

Para passar esse arquivo para JVM executar basta rodar o comando:  
`java OlaDevDojo`

Referência: [05 - Executando compilação manualmente](https://www.youtube.com/watch?v=E64JTsEyXCM&list=PL62G310vn6nFIsOCC0H-C2infYgwm8SWW&index=6)  